package com.zuitt.wdc044.models;

import javax.persistence.*;

// mark this Java Object as a representation of a database table via @Entity annotation
@Entity

@Table(name = "posts")
public class Post {
    //Indicates primary key
    @Id
    //Indicates auto-increment
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    //Constructors
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public Post(String title, String content, User user){
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public String getTitle(){ return this.title; }
    public void setTitle(String title) { this.title = title; }

    public String getContent(){ return this.content; }
    public void setContent(String content) { this.content = content; }

    public User getUser(){ return this.user; }
    public void setUser(User user) { this.user = user; }

    public Long getId(){ return this.id; }


}