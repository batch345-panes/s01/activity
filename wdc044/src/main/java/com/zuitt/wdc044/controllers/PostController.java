package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<?> createPost(@RequestBody Post post, @RequestHeader(value = "Authorization") String stringToken){
        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post sucessfully created", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePosts(@PathVariable Long postid, @RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable Long postid, @RequestHeader(value="Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }

    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getPosts(stringToken), HttpStatus.OK);
    }
}