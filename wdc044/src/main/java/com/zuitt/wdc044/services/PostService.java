package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(String token, Post post);

    Iterable<Post> getPosts();

    Iterable<Post> getPosts(String token);

    ResponseEntity<?> updatePost(Long id, String stringToken, Post post);

    public ResponseEntity<?> deletePost(Long id, String stringToken);
}