package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import java.util.Optional;

public interface UserService {
    void createUser(User user);


    //default service wherein it will return the record that will match the username provided in the parameter, otherwise Optional returns null
    Optional<User> findByUsername(String username);
}