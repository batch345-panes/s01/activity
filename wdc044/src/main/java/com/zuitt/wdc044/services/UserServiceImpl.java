package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    //Dependency injection
    //@Autowired - instance of a class that implements
    //When it comes to the UserRepository
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }


    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable((userRepository.findByUsername(username)));
    }
}

//NOTES:

// a. Creating a spring boot project
// Go to https://start.spring.io/ to initialize a spring boot project
// Set up the following:
// Project: Maven
// Language: Java
// Spring Boot: 2.7.15 (or the most previous update)
// Artifact & Name: projectName
// Group: com.nameOfThePackage
// Packaging: War (for creating web apps)
// Java: 11 (jdk version) / or the current installed jdk in your device
// Under Dependencies: Click Add Dependencies > Spring Web
// Click Generate
//b. Extract the Zip file in your batch folder and open the file in intellij
//c. Look for the pom.xml and do the following:
// Right Click pom.xml > Look for Maven > Download Sources > Wait for the  download to be done.
// Right Click again pom.xml > Maven >  Reload Project > This should fix any errors on first time of opening the project.
// Note: the step "c" should also be followed if new dependencies is added on the pom.xml

// d. Command for running the application:
// ./mvnw spring-boot:run
// Note: This will also allow us to serve the Springboot project on tomcat server.