package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;


@Service
public class PostServiceImpl implements PostService{
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public void createPost(String token, Post post) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepository.findByUsername(username);
        Post newPost = new Post(post.getTitle(), post.getContent(), author);

        postRepository.save(newPost);
    }

    @Override
    public Iterable<Post> getPosts() {
        Iterable<Post> allPosts = postRepository.findAll();
        for(Post post : allPosts){
            post.getUser().setPassword("");
        }
        return allPosts;
    }

    @Override
    public Iterable<Post> getPosts(String token) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepository.findByUsername(username);
        Set<Post> setPost = author.getPosts();
        for(Post post : setPost){
            post.getUser().setPassword("");
        }
        return setPost;
    }

    @Override
    public ResponseEntity<?> updatePost(Long id, String stringToken, Post post) {
        Optional<Post> optionalPostForUpdate = postRepository.findById(id);

        if(optionalPostForUpdate.isEmpty()){ return new ResponseEntity<>("Post Not Found", HttpStatus.BAD_REQUEST); }

        Post postForUpdate = optionalPostForUpdate.get();
        String postAuthor = postForUpdate.getUser().getUsername();
        String authenticatedAuthor = jwtToken.getUsernameFromToken(stringToken);

        if(postAuthor.equals(authenticatedAuthor)){
            postForUpdate.setContent(post.getContent());
            postForUpdate.setTitle(post.getTitle());
            postRepository.save(postForUpdate);
            return new ResponseEntity<>("Post updated sucessfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<?> deletePost(Long id, String stringToken) {
        Optional<Post> postForDelete = postRepository.findById(id);

        if(postForDelete.isEmpty()){ return new ResponseEntity<>("Post Not Found", HttpStatus.BAD_REQUEST); }

        String postAuthor = postForDelete.get().getUser().getUsername();
        String authenticatedAuthor = jwtToken.getUsernameFromToken(stringToken);

        if(postAuthor.equals(authenticatedAuthor)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted sucessfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }
    }


}